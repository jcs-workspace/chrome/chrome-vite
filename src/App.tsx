import './App.css';
import Header from './components/Header';
import Chart from './components/Chart';

function App() {

  return (
    <>
      <Header />
      <Chart />
    </>
  );
}

export default App;
