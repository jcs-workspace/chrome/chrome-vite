// @ts-expect-error The author doesn't include type declaration file.
import { EChart } from '@hcorta/react-echarts';

function Chart() {
    return (
        <EChart
            renderer={'svg'}
            onClick={() => console.log('clicked!')}
            xAxis={{
                type: 'category'
            }}
            yAxis={{
                type: 'value',
                boundaryGap: [0, '30%']
            }}
            series={[
                {
                    type: 'line',
                    data: [
                        ['2022-10-17', 300],
                        ['2022-10-18', 100]
                    ]
                }
            ]}
        />
    )
}

export default Chart;
