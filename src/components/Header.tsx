import reactLogo from '../assets/react.svg';
import Toggle from './Toggle';

function Header() {

    return (
        <>
            <div>
                <img src={reactLogo} className="logo react" alt="React logo" />
                <h2>Hello World</h2>
            </div>
            <Toggle />
        </>
    );
}

export default Header;
